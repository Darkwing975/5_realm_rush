using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestruct : MonoBehaviour
{
    [SerializeField] private float selfDestructTime = 5f;
    // Start is called before the first frame update
    private void Start()
    {
        Destroy(gameObject, selfDestructTime);
    }

    // Update is called once per frame
    private void Update()
    {
        
    }
}
