using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using UnityEngine;
using TMPro;
public class PlayerHealth : MonoBehaviour
{
    [SerializeField] private float health;
    [SerializeField] private float healthDecrease = 1f;
    [SerializeField] private TextMeshProUGUI healthText;
    [SerializeField] private AudioClip baseDamage;

    private void Start()
    {
        healthText.text = health.ToString();
    }

    private void OnTriggerEnter(Collider other)
    {
        GetComponent<AudioSource>().PlayOneShot(baseDamage);
        health = health - healthDecrease;
        healthText.text = health.ToString();
    }
}
    